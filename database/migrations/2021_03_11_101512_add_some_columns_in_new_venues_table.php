<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSomeColumnsInNewVenuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('new_venues', function (Blueprint $table) {
            $table->text('center_type')->nullable()->after('comments_notes');
            $table->text('managers_name')->nullable()->after('center_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('new_venues', function (Blueprint $table) {
            $table->dropColumn(['center_type']);
            $table->dropColumn(['managers_name']);
        });
    }
}
