<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSubcolumnToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->enum('user_type',['0', '1'])->nullable()->index()->comment('0 - Admins, 1 - Customers')->after('mobile_no');
            $table->string('profile',255)->nullable()->after('user_type');
            $table->enum('gender',['0', '1'])->nullable()->index()->comment('0 - Female, 1 - Male')->after('profile');
            $table->date('dob')->nullable()->after('gender');
            $table->unsignedInteger('country_id')->index()->nullable()->comment('countries table id')->after('dob');
            $table->foreign('country_id')->references('id')->on('countries');
            $table->unsignedInteger('state_id')->index()->nullable()->comment('states table id')->after('country_id');
            $table->foreign('state_id')->references('id')->on('states');
            $table->unsignedInteger('city_id')->index()->nullable()->comment('cities table id')->after('state_id');
            $table->foreign('city_id')->references('id')->on('cities');
            $table->enum('status', ['0', '1'])->index()->comment('0 - Inactive, 1 - Active')->after('address');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['user_type']);
            $table->dropColumn(['profile']);
            $table->dropColumn(['gender']);
            $table->dropColumn(['dob']);
            $table->dropForeign(['country_id']);
            $table->dropIndex(['country_id']);
            $table->dropColumn(['country_id']);
            $table->dropForeign(['state_id']);
            $table->dropIndex(['state_id']);
            $table->dropColumn(['state_id']);
            $table->dropForeign(['city_id']);
            $table->dropIndex(['city_id']);
            $table->dropColumn(['city_id']);
            $table->dropColumn(['status']);

        });
    }
}
