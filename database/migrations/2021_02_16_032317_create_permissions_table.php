<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissions', function (Blueprint $table) {
            $table->increments('id')->index()->comment('AUTO_INCREMENT');
            $table->string('name',255)->nullable();
            $table->string('label',255)->nullable();
            $table->string('guard_name',255)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedInteger('created_by')->nullable()->comment('Users table ID');
            $table->unsignedInteger('updated_by')->nullable()->comment('Users table ID');
        });

        DB::table('permissions')->insert([

            array('id' => 1,'name' => 'users','label' => 'User','guard_name' => 'root','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 2,'name' => 'countries','label' => 'Country','guard_name' => 'root','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 3,'name' => 'states','label' => 'State','guard_name' => 'root','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 4,'name' => 'cities','label' => 'City','guard_name' => 'root','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 5,'name' => 'hobbies','label' => 'Hobby','guard_name' => 'root','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 6,'name' => 'roles','label' => 'Role','guard_name' => 'root','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 7,'name' => 'permissions','label' => 'Permission','guard_name' => 'root','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 8,'name' => 'permission-role-mappings','label' => 'Permission Role Mapping','guard_name' => 'root','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),

            array('id' => 9,'name' => 'index-users','label' => 'View','guard_name' => 'users','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 10,'name' => 'show-users','label' => 'Show','guard_name' => 'users','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 11,'name' => 'store-users','label' => 'Add','guard_name' => 'users','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 12,'name' => 'update-users','label' => 'Update','guard_name' => 'users','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 13,'name' => 'destroy-users','label' => 'Delete','guard_name' => 'users','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 14,'name' => 'export-users','label' => 'Export','guard_name' => 'users','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 15,'name' => 'importBulk-users','label' => 'Import','guard_name' => 'users','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 16,'name' => 'deleteAll-users','label' => 'Delete All','guard_name' => 'users','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 17,'name' => 'delete_gallery-users','label' => 'Delete Gallery','guard_name' => 'users','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),

            array('id' => 18,'name' => 'index-countries','label' => 'View','guard_name' => 'countries','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 19,'name' => 'show-countries','label' => 'Show','guard_name' => 'countries','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 20,'name' => 'store-countries','label' => 'Add','guard_name' => 'countries','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 21,'name' => 'update-countries','label' => 'Update','guard_name' => 'countries','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 22,'name' => 'destroy-countries','label' => 'Delete','guard_name' => 'countries','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 23,'name' => 'export-countries','label' => 'Export','guard_name' => 'countries','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 24,'name' => 'importBulk-countries','label' => 'Import','guard_name' => 'countries','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 25,'name' => 'deleteAll-countries','label' => 'Delete All','guard_name' => 'countries','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),

            array('id' => 26,'name' => 'index-states','label' => 'View','guard_name' => 'states','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 27,'name' => 'show-states','label' => 'Show','guard_name' => 'states','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 28,'name' => 'store-states','label' => 'Add','guard_name' => 'states','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 29,'name' => 'update-states','label' => 'Update','guard_name' => 'states','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 30,'name' => 'destroy-states','label' => 'Delete','guard_name' => 'states','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 31,'name' => 'export-states','label' => 'Export','guard_name' => 'states','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 32,'name' => 'importBulk-states','label' => 'Import','guard_name' => 'states','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 33,'name' => 'deleteAll-states','label' => 'Delete All','guard_name' => 'states','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),

            array('id' => 34,'name' => 'index-cities','label' => 'View','guard_name' => 'cities','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 35,'name' => 'show-cities','label' => 'Show','guard_name' => 'cities','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 36,'name' => 'store-cities','label' => 'Add','guard_name' => 'cities','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 37,'name' => 'update-cities','label' => 'Update','guard_name' => 'cities','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 38,'name' => 'destroy-cities','label' => 'Delete','guard_name' => 'cities','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 39,'name' => 'export-cities','label' => 'Export','guard_name' => 'cities','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 40,'name' => 'importBulk-cities','label' => 'Import','guard_name' => 'cities','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 41,'name' => 'deleteAll-cities','label' => 'Delete All','guard_name' => 'cities','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),

            array('id' => 42,'name' => 'index-hobbies','label' => 'View','guard_name' => 'hobbies','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 43,'name' => 'show-hobbies','label' => 'Show','guard_name' => 'hobbies','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 44,'name' => 'store-hobbies','label' => 'Add','guard_name' => 'hobbies','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 45,'name' => 'update-hobbies','label' => 'Update','guard_name' => 'hobbies','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 46,'name' => 'destroy-hobbies','label' => 'Delete','guard_name' => 'hobbies','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 47,'name' => 'export-hobbies','label' => 'Export','guard_name' => 'hobbies','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 48,'name' => 'importBulk-hobbies','label' => 'Import','guard_name' => 'hobbies','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 49,'name' => 'deleteAll-hobbies','label' => 'Delete All','guard_name' => 'hobbies','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),

            array('id' => 50,'name' => 'index-roles','label' => 'View','guard_name' => 'roles','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 51,'name' => 'show-roles','label' => 'Show','guard_name' => 'roles','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 52,'name' => 'store-roles','label' => 'Add','guard_name' => 'roles','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 53,'name' => 'update-roles','label' => 'Update','guard_name' => 'roles','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 54,'name' => 'destroy-roles','label' => 'Delete','guard_name' => 'roles','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 55,'name' => 'export-roles','label' => 'Export','guard_name' => 'roles','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 56,'name' => 'deleteAll-roles','label' => 'Delete All','guard_name' => 'roles','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),

            array('id' => 57,'name' => 'index-permissions','label' => 'View','guard_name' => 'permissions','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 58,'name' => 'show-permissions','label' => 'Show','guard_name' => 'permissions','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 59,'name' => 'store-permissions','label' => 'Add','guard_name' => 'permissions','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 60,'name' => 'update-permissions','label' => 'Update','guard_name' => 'permissions','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 61,'name' => 'destroy-permissions','label' => 'Delete','guard_name' => 'permissions','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 62,'name' => 'export-permissions','label' => 'Export','guard_name' => 'permissions','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 63,'name' => 'deleteAll-permissions','label' => 'Delete All','guard_name' => 'permissions','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),

            array('id' => 64,'name' => 'setUnsetPermissionToRole-permissions','label' => 'Set/Unset Permission','guard_name' => 'permission-role-mappings','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),
            array('id' => 65,'name' => 'getPermissionsByRole-roles','label' => 'Permissions By Role','guard_name' => 'permission-role-mappings','created_at' => config('constants.calender.date_time'),'updated_at' => config('constants.calender.date_time')),

        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permissions');
    }
}
