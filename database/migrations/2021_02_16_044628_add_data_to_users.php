<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDataToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
        DB::table('users')->insert([
            [
                'name' => 'Administrator',
                'email'=>'administrator@gmail.com',
                'password'=>'$2y$10$gCb4kNHFsGHu.hgvMo5.W.sI/my48gC9OTVSbwTT7aOnY/kpidUHK', // 123456
                'mobile_no'=>'1234567890',
                'user_type'=>'0',
                'role_id'=>'1',
                'status'=>'1',
                'email_verified_at'=> config('constants.calender.date_time'),
                'created_at' => config('constants.calender.date_time'),
                'updated_at' => config('constants.calender.date_time')
            ],

            [
                'name' => 'Sub Admin',
                'email'=>'subadmin@gmail.com',
                'password'=>'$2y$10$gCb4kNHFsGHu.hgvMo5.W.sI/my48gC9OTVSbwTT7aOnY/kpidUHK', // 123456
                'mobile_no'=>'1234567890',
                'user_type'=>'0',
                'role_id'=>'2',
                'status'=>'1',
                'email_verified_at'=> config('constants.calender.date_time'),
                'created_at' => config('constants.calender.date_time'),
                'updated_at' => config('constants.calender.date_time')
            ],

            [
                'name' => 'Customer',
                'email'=>'contact@gmail.com',
                'password'=>'$2y$10$gCb4kNHFsGHu.hgvMo5.W.sI/my48gC9OTVSbwTT7aOnY/kpidUHK', // 123456
                'mobile_no'=>'1234567890',
                'user_type'=>'1',
                'role_id'=> NULL,
                'status'=>'0',
                'email_verified_at'=> NULL,
                'created_at' => config('constants.calender.date_time'),
                'updated_at' => config('constants.calender.date_time')
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
