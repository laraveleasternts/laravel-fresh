<?php

namespace App\Http\Controllers;

use App\Models\Venue;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class VenueController extends Controller
{
    public function handle()
    {
        $google_api_key = "AIzaSyC6Ebl-s6AVB5jcYshKN-343Ld9wR-as2A";
        $client = new Client();
        $venues = Venue::all();

        foreach ($venues as $venue) {
            try {
                print_r($venue->id . ' ');
                $res = $client->get('https://maps.googleapis.com/maps/api/place/details/json?key=' . $google_api_key . '&placeid=' . $venue->g_place_id);
                if ($res->getStatusCode() == 200) {

                    // body -> https://developers.google.com/places/web-service/details#PlaceDetailsResults
                    $body = $res->getBody();
                    $body = json_decode($body);
                    if (isset($body->result)) {
                        $result = $body->result;
                        if (isset($result->business_status)
                            && strtoupper($result->business_status) == strtoupper("CLOSED_PERMANENTLY")) {
                            $venue->permanently_closed = '1'; // set the flag to '1' for venue close
                        }
                        //if created_by is NULL than set it to admin (webmaster email id) whose id is 0
                        if (is_null($venue->created_by)) {
                            $venue->created_by = '1';
                        }
                        if (isset($result->formatted_address) && strlen($result->formatted_address) > 0) {
                            $venue->physical_address = $result->formatted_address;
                        }
                        if (isset($result->international_phone_number) && strlen($result->international_phone_number) > 0) {
                            $venue->telephone = $result->international_phone_number;
                        }
                        if (isset($result->name) && strlen($result->name) > 0) {
                            $venue->name = $result->name;
                        }
                        if (isset($result->url) && strlen($result->url) > 0) {
                            $venue->g_map_url = $result->url;
                        }
                        if (isset($result->website) && strlen($result->website) > 0) {
                            $venue->website = $result->website;
                        }
                        if (isset($result->types)) {
                            $types = $result->types;
                            $venue->types = implode(';', $types);
                        }
                        //set the latitude & longitude
                        if (isset($result->formatted_address->geometry)) {
                            if (isset($result->formatted_address->geometry->location)) {
                                $location = $result->formatted_address->geometry->location;
                                if (isset($location->lat) && strlen($location->lat) > 0) {
                                    $venue->latitude = $location->lat;
                                }
                                if (isset($location->lng) && strlen($location->lng) > 0) {
                                    $venue->longitude = $location->lng;
                                }
                            }
                        }
                        //set the country, country_code, state & suburb
                        if (isset($result->address_components)) {
                            $address_components = $result->address_components;
                            foreach ($address_components as $address_component) {
                                $types = $address_component->types;
                                foreach ($types as $type) {
                                    // dont sync country, state & suburb for UK (England, Northern Ireland, Scotland, Wales) & Ireland
                                    if (($venue->country_code != 'GB')
                                        && ($venue->country_code != 'IE')) {
                                        if ($type == 'country') {
                                            $venue->country = $address_component->long_name;
                                            $venue->country_code = $address_component->short_name;
                                            break;
                                        }
                                        if ($type == 'administrative_area_level_1') {
                                            $venue->state = $address_component->long_name;
                                            break;
                                        }
                                        if ($type == 'postal_code') {
                                            $venue->postal_address = $address_component->long_name;
                                            break;
                                        }
                                        if (($type == 'locality')
                                            || ($type == 'postal_town')
                                            || ($type == 'administrative_area_level_3')) {
                                            $venue->suburb = $address_component->long_name;
                                            break;
                                        }
                                    }
                                } // end of types foreach
                            } // end of address_components foreach
                        }
                        if (isset($result->photos)) {
                            $photos = $result->photos;
                            foreach ($photos as $photo) {
                                $width = $photo->width;
//                                $height = $photo->height;
                                $photo_reference = $photo->photo_reference;

                                $venue->venue_image = 'https://maps.googleapis.com/maps/api/place/photo?maxwidth=' . $width . '&photoreference=' . $photo_reference . '&key=' . $google_api_key;

                                break; //capture only the first object
                            }
                        }
                        $venue->last_sync_at = Carbon::now()->toDateTimeString();
                        $venue->update();
                    } //if body found
                } // if status == 200
            } catch (\Exception $e) {
                print_r($e);
            }
        } //end of venues foreach
    }
}
