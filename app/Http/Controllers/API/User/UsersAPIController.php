<?php

namespace App\Http\Controllers\API\User;

use App\Events\ManageUser;
use App\Exports\User\UsersExport;
use App\Http\Requests\User\UserUpdateRequest;
use App\Http\Resources\DataTrueResource;
use App\User;
use App\Models\User\UserGallery;
use App\Http\Requests\User\UsersRequest;
use App\Http\Resources\User\UsersCollection;
use App\Http\Resources\User\UsersResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use App\Traits\UploadTrait;
use URL;

/*
|--------------------------------------------------------------------------
| Users Controller
|--------------------------------------------------------------------------
|
| This controller handles the Roles of
register,
index,
show,
store,
update,
destroy,
export Methods.
|
*/

class UsersAPIController extends Controller
{
    use UploadTrait;

    /***
     * Register New User
     * @param UsersRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(UsersRequest $request)
    {
        return User::Register($request, config('constants.user.user_type_enum.1'));
    }

    /**
     * List All Users
     * @param Request $request
     * @return UsersCollection
     */
    public function index(Request $request)
    {
        $relation = [
            'user_galleries',
            'hobbies',
            'role',
            'country',
            'state',
            'city'
        ];

        if ($request->get('is_light', false)) {
            $user = new User();
            $query = User::commonFunctionMethod(User::select($user->light), $request, true);
        } else
            $query = User::commonFunctionMethod(User::with($relation), $request, true);

        return new UsersCollection(UsersResource::collection($query), UsersResource::class);
    }

    /**
     * Users detail
     * @param User $user
     * @return UsersResource
     */
    public function show(User $user)
    {
        return new UsersResource($user->load([]));
    }

    /**
     * Create User
     *
     * @param UsersRequest $request
     * @return mixed
     */
    public function store(UsersRequest $request)
    {
        return User::Register($request, config('constants.user.user_type_enum.0'));

    }

    /**
     * Update Users
     * @param UsersRequest $request
     * @param User $user
     * @return UsersResource
     */
    public function update(UserUpdateRequest $request, User $user)
    {
        return User::UpdateUser($request, $user);

    }

    /**
     * Delete User
     *
     * @param Request $request
     * @param User $user
     * @return DataTrueResource|\Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Request $request, User $user)
    {
        if ($user->id == config('constants.system_user_id'))
            return User::GetError(config('constants.messages.admin_user_delete_error'));


        $user->hobbies()->detach();

        $realPath = 'user/' . $user->id . '/';
        Storage::deleteDirectory('/public/' . $realPath);
        UserGallery::where('user_id', $user->id)->delete();

        Storage::deleteDirectory('/public/' . $realPath);
        $user->delete();

        User::deleteSingle($user);

        event(new ManageUser($user, config('constants.broadcasting.operation.delete')));

        return new DataTrueResource($user);
    }

    /**
     * Delete User multiple
     * @param Request $request
     * @return DataTrueResource
     */
    public function deleteAll(Request $request)
    {
        return User::deleteAll($request);
    }

    /**
     * Export Users Data
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export(Request $request)
    {
        return Excel::download(new UsersExport($request), 'user.csv');
    }

    /**
     * Delete gallery
     * @param Request $request
     * @param UserGallery $gallery
     * @return DataTrueResource
     * @throws \Exception
     */
    public function delete_gallery(Request $request, UserGallery $gallery)
    {
        $this->deleteOne('/public/user/' . $gallery->user_id . '/' . basename($gallery->filename));
        $gallery->delete();

        return new DataTrueResource($gallery);
    }

    /**
     * This is a batch request API
     *
     * @param Request $requestObj
     * @return \Illuminate\Http\JsonResponse
     */
    public function batchRequest(Request $requestObj)
    {
        dd('hi');
        $requests = $requestObj->get('request');//get request
        $output = array();
        $cnt = 0;
        foreach ($requests as $request) {// foreach for all requests inside batch

            $request = (object)$request;// array request convert to object

            if ($cnt == 10)// limit maximum call 10 requests
                break;

            $url = parse_url($request->url);

//querystrings code
            $query = array();
            if (isset($url['query'])) {
                parse_str($url['query'], $query);
            }

            $server = ['HTTP_HOST' => preg_replace('#^https?://#', '', URL::to('/')), 'HTTPS' => 'on'];
            $req = Request::create($request->url, 'GET', $query, [], [], $server);// set request

            $req->headers->set('Accept', 'application/json');//set accept header
            $res = app()->handle($req);//call request

            if (isset($request->request_id)) {// check request_id is set or not
                $output[$request->request_id] = json_decode($res->getContent()); // get response and set into output array
            } else {
                $output[] = $res;
            }

            $cnt++;// request counter
        }

        return response()->json(array('response' => $output));// return batch response
    }


}
