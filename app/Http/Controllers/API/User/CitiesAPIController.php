<?php

namespace App\Http\Controllers\API\User;

use App\Events\ManageCity;
use App\Exports\User\CitiesExport;
use App\Http\Resources\DataTrueResource;
use App\User;
use App\Models\User\City;
use App\Http\Requests\User\CitiesRequest;
use App\Http\Resources\User\CitiesCollection;
use App\Http\Resources\User\CitiesResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

/*
   |--------------------------------------------------------------------------
   | Cities Controller
   |--------------------------------------------------------------------------
   |
   | This controller handles the Roles of
     index,
     show,
     store,
     update,
     destroy,
     export and
     importBulk Methods.
   |
   */

class CitiesAPIController extends Controller
{
    /**
     * list Cities
     * @param Request $request
     * @return CitiesCollection
     */
    public function index(Request $request)
    {
        if($request->get('is_light',false)){
            $city = new City();
            $query = User::commonFunctionMethod(City::select($city->light),$request,true);
        }
        else
            $query = User::commonFunctionMethod(City::with(['state']),$request,true);

        return new CitiesCollection(CitiesResource::collection($query),CitiesResource::class);
    }

    /**
     * City Detail
     * @param City $city
     * @return CitiesResource
     */
    public function show(City $city)
    {
        return new CitiesResource($city->load([]));
    }

    /**
     * add City
     * @param CitiesRequest $request
     * @return CitiesResource
     */
    public function store(CitiesRequest $request)
    {
        $city = City::create($request->all());
        event(new ManageCity($city,config('constants.broadcasting.operation.add')));
        return new CitiesResource($city);
    }

    /**
     * Update City
     * @param CitiesRequest $request
     * @param City $city
     * @return CitiesResource
     */
    public function update(CitiesRequest $request, City $city)
    {
        $city->update($request->all());
        event(new ManageCity($city,config('constants.broadcasting.operation.edit')));
        return new CitiesResource($city);
    }

    /**
     * Delete City
     *
     * @param Request $request
     * @param City $city
     * @return DataTrueResource|\Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Request $request, City $city)
    {
        $inUse = City::commonCodeForDeleteModelRestrictions([User::class],'city_id',[$city->id]);
        if(!empty($inUse))
            return User::GetError("City is unable to delete because it's used in [".implode(",",$inUse)."].");

        $city->delete();
        event(new ManageCity($city,config('constants.broadcasting.operation.delete')));
        return new DataTrueResource($city);
    }

    /**
     * Delete City multiple
     * @param Request $request
     * @return DataTrueResource
     */
    public function deleteAll(Request $request)
    {
        return City::deleteAll($request);
    }
    /**
     * Export City Data
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export(Request $request)
    {
        return Excel::download(new CitiesExport($request), 'city.csv');
    }
}
