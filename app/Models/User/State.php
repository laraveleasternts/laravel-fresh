<?php

namespace App\Models\User;
use App\Events\ManageState;
use App\Http\Resources\DataTrueResource;
use App\Traits\Scopes;
use App\Traits\CreatedbyUpdatedby;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class State extends Model
{
    use SoftDeletes, Scopes,CreatedbyUpdatedby;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'id', 'name','country_id'
    ];

    /**
     * Lightweight response variable
     *
     * @var array
     */
    public $light = [
        'id', 'name'
    ];

    /**
     * @var array
     */
    public $sortable=[
        'name'
    ];

    /**
     * @var array
     */
    public $foreign_sortable = [
        'country_id'
    ];

    /**
     * @var array
     */
    public $foreign_table = [
        'countries'
    ];

    /**
     * @var array
     */
    public $foreign_key = [
        'name'
    ];

    /**
     * @var array
     */
    public $foreign_method = [
        'country'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //
        'id'=>'string',
        'country_id'=>'string',
        'name'=>'string',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country() {
        return $this->belongsTo(Country::class);
    }

    /**
     * Multiple Delete
     * @param $query
     * @param $request
     * @return DataTrueResource|\Illuminate\Http\JsonResponse
     */
    public function scopeDeleteAll($query,$request){
        if(!empty($request->id)) {

            $inUse = State::commonCodeForDeleteModelRestrictions([User::class,City::class],'state_id',$request->id);
            if(!empty($inUse))
                return User::GetError("State is unable to delete because it's used in [".implode(",",$inUse)."].");

            State::whereIn('id', $request->id)->delete();

            event(new ManageState($request->id,config('constants.broadcasting.operation.delete_multiple')));

            return new DataTrueResource(true);
        }
        else{
            return User::GetError(config('constants.messages.delete_multiple_error'));
        }
    }
}
