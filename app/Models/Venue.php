<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Venue extends Model
{
    public $fillable = [
        'name',
        'physical_address',
        'suburb',
        'state',
        'country',
        'country_code',
        'postal_address',
        'telephone',
        'website',
        'email',
        'fb_page_url',
        'g_place_id',
        'latitude',
        'longitude',
        'elevation',
        'g_map_url',
        'member_only',
        'no_of_glass_courts',
        'no_of_non_glass_courts',
        'no_of_courts',
        'membership_fees',
        'court_rental_fees',
        'peak_times',
        'club_shop',
        'sports_shop',
        'hot_shower',
        'gym',
        'multi_sport_venue',
        'cafe',
        'serves_alcohol',
        'water_fountain',
        'drink_vending_machine',
        'snack_vending_machine',
        'free_parking',
        'online_booking',
        'twentyfour_hour_access',
        'permanently_staffed',
        're_stringing_service',
        'defibrillator',
        'first_aid_kit',
        'types',
        'category',
        'status',
        'reason_for_deletion',
        'deletion_request_by_user_id',
        'last_sync_at',
    ];

}
