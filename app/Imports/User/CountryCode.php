<?php

namespace App\Imports\User;

use App\Models\User\Country;
use App\Models\User\SquashImport;
use App\Traits\Scopes;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Facades\Validator;
use App\Traits\CreatedbyUpdatedby;
use Illuminate\Support\Facades\Http;

class CountryCode implements ToCollection, WithStartRow
{
    use Scopes, CreatedbyUpdatedby;

    private $errors = [];
    private $rows = 0;

    public function startRow(): int
    {
        return 2;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function rules(): array
    {
        /* return [
             '0' => 'required|max:191|unique:countries,name,NULL,id,deleted_at,NULL'
         ];*/
    }

    public function validationMessages()
    {
        return [
            /*'0.required' => trans('The name is required'),
            '0.max' => trans('The name may not be greater than 255 characters'),
            '0.unique' => trans('The name has already been taken'),*/
        ];
    }

    public function validateBulk($collection)
    {
        $i = 1;
        foreach ($collection as $col) {
            $i++;
            $validator = Validator::make($col->toArray(), $this->rules(), $this->validationMessages());
            if ($validator->fails()) {
                foreach ($validator->errors()->messages() as $messages) {
                    foreach ($messages as $error) {
                        $this->errors[] = $error . ' on row ' . $i;
                    }
                }
            }
        }
        return $this->getErrors();
    }

    public function collection(Collection $collection)
    {

        /*$error = $this->validateBulk($collection);
        if($error){
            return;
        }else {*/
        foreach ($collection as $col) {
            $input = urlencode($col[0]);
            $url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" . $input . "&key=AIzaSyC6Ebl-s6AVB5jcYshKN-343Ld9wR-as2A";
            $response = Http::get($url);
            $content = json_decode($response);

            if ($content->status == "OK") {

                $predictions = $content->predictions;
                $place_id = $predictions[0]->place_id;

                SquashImport::create([
                    'venue' => $col[0],
                    'node_count' => $col[1],
                    'region' => $col[2],
                    'google_map' => $col[3],
                    'place_id' => $place_id
                ]);
            }
            $this->rows++;

        }
    }

//}

    public
    function getRowCount(): int
    {
        return $this->rows;
    }
}

