<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::get('email/verify/{id}', 'API\User\VerificationAPIController@verify')->name('verification.verify');
Route::get('email/resend', 'API\User\VerificationAPIController@resend')->name('verification.resend');

Route::post('forgot-passsword','API\User\ForgotPasswordAPIController@sendResetLinkEmail');

Route::post('register', 'API\User\UsersAPIController@register');

Route::get('batch_request', 'API\User\UsersAPIController@batchRequest');
Route::get('all-venue', 'VenueController@handle');

Route::post('login', 'API\User\LoginController@login');

Route::post('upload-image', 'API\User\ImageUploadController@postUploadForm');
Route::get('gen-image/{image}', 'API\User\ImageUploadController@generateCustomImage'); // Generate custom image dynamically

Route::group([
    'middleware' => ['auth:api', 'check.permission'],
], function () {

    Route::resource('countries', 'API\User\CountriesAPIController');
    Route::post('countries-delete-multiple', 'API\User\CountriesAPIController@deleteAll');
    Route::get('countries-export', 'API\User\CountriesAPIController@export');
    Route::post('countries-import-bulk', 'API\User\CountriesAPIController@importBulk');
    Route::post('squash-import-bulk', 'API\User\CountriesAPIController@squashImportBulk');



    Route::resource('states', 'API\User\StatesAPIController');
    Route::post('states-delete-multiple', 'API\User\StatesAPIController@deleteAll');
    Route::get('states-export', 'API\User\StatesAPIController@export');


    Route::resource('cities', 'API\User\CitiesAPIController');
    Route::post('cities-delete-multiple', 'API\User\CitiesAPIController@deleteAll');
    Route::get('cities-export', 'API\User\CitiesAPIController@export');


    Route::resource('hobbies', 'API\User\HobbiesAPIController');
    Route::post('hobbies-delete-multiple', 'API\User\HobbiesAPIController@deleteAll');
    Route::get('hobbies-export', 'API\User\HobbiesAPIController@export');


    Route::post('users/{user}', 'API\User\UsersAPIController@update');
    Route::get('users/{user}', 'API\User\UsersAPIController@show');
    Route::get('users', 'API\User\UsersAPIController@index');
    Route::delete('users-delete/{user}', 'API\User\UsersAPIController@destroy');
    Route::post('users-delete-multiple', 'API\User\UsersAPIController@deleteAll');
    Route::post('users-import-bulk', 'UsersAPIController@importBulk');
    Route::get('users-export', 'API\User\UsersAPIController@export');

    Route::apiResource('roles', 'API\User\RolesAPIController');
    Route::get('roles-export', 'API\User\RolesAPIController@export');
    Route::get('get_role_by_permissions/{id}', 'API\User\RolesAPIController@getPermissionsByRole');
    Route::post('roles-delete-multiple', 'API\User\RolesAPIController@deleteAll');

    Route::apiResource('permissions', 'API\User\PermissionsAPIController');
    Route::post('permissions-delete-multiple', 'API\User\PermissionsAPIController@deleteAll');
    Route::get('permissions-export', 'API\User\PermissionsAPIController@export');

    Route::post('set_unset_permission_to_role', 'API\User\PermissionsAPIController@setUnsetPermissionToRole');

    Route::delete('gallery/{gallery}', 'API\User\UsersAPIController@delete_gallery');

    Route::post('change-password', 'API\User\LoginController@changePassword');

    Route::get('logout', 'API\User\LoginController@logout');

    Route::resource('import-csv-log', 'API\User\ImportCsvLogsAPIController');


});
